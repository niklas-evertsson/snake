﻿using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] float startDelay = 3f;
    [SerializeField] float endDelay = 3f;
    [SerializeField] string mainMenuSceneName = "MainMenu";
    [SerializeField] BoolVariable highScoreRound = null;
    [SerializeField] IntVariablePositive bestScore = null;
    [SerializeField] IntVariablePositive score = null;
    [SerializeField] GameEvent OnRoundStart = null;
    [SerializeField] GameEvent OnRoundPlay = null;
    [SerializeField] GameEvent OnRoundEnd = null;

    bool endRound = false;
    WaitForSeconds startWait;
    WaitForSeconds endWait;

    public void EndRound()
    {
        endRound = true;
    }

    void Start()
    {
        startWait = new WaitForSeconds(startDelay);
        endWait = new WaitForSeconds(endDelay);
        StartCoroutine(GameLoop());
    }

    IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());
        yield return StartCoroutine(ApplicationController.LoadSceneAsync(mainMenuSceneName));
    }

    IEnumerator RoundStarting()
    {
        OnRoundStart.Raise();
        yield return startWait;
    }

    IEnumerator RoundPlaying()
    {
        endRound = false;
        OnRoundPlay.Raise();
        while(!endRound)
        {
            yield return null;
        }
    }

    IEnumerator RoundEnding()
    {
        if(score.Value > bestScore.Value)
        {
            bestScore.Set(score.Value);
            highScoreRound.Value = true;
        }
        OnRoundEnd.Raise();
        yield return endWait;
    }
}

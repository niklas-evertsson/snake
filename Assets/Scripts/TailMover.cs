﻿using System.Collections.Generic;
using UnityEngine;

public class TailMover : MonoBehaviour
{
    [SerializeField] int tailSegmentsAtStart = 2;
    [SerializeField] IntVariablePositive foodInBelly = null;
    [SerializeField] TailSegment tailSegmentPrefab = null;

    Queue<TailSegment> tail = new Queue<TailSegment>();

    public void MoveTail(Vector2 position)
    {
        if(tail.Count > 0)
        {
            TailSegment tailEnd;
            if(foodInBelly.Value > 0)
            {
                foodInBelly.Subtract(1);
                tailEnd = AddTailSegment();
            }
            else
            {
                tailEnd = tail.Dequeue();
                tail.Enqueue(tailEnd);
            }
            tailEnd.transform.position = position;
            tailEnd.EnableCollision();
        }
    }

    void Awake()
    {
        CreateTail();
    }

    TailSegment AddTailSegment()
    {
        TailSegment tailSegment = Instantiate(tailSegmentPrefab, transform.position, Quaternion.identity);
        tail.Enqueue(tailSegment);
        return tailSegment;
    }

    void CreateTail()
    {
        tail.Clear();
        for(int i = 0; i < tailSegmentsAtStart; i++)
        {
            AddTailSegment();
        }
    }
}

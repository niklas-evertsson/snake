﻿using UnityEngine;
using UnityEngine.UI;

public class UITextEnabler : MonoBehaviour
{
    [SerializeField] BoolVariable condition = null;

    Text textComponent;

    public void EnableText()
    {
        if(condition.Value)
        {
            textComponent.enabled = true;
        }
    }

    void Awake()
    {
        textComponent = GetComponent<Text>();
    }
}

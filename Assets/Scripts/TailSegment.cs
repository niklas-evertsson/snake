﻿using UnityEngine;

public class TailSegment : MonoBehaviour
{
    BoxCollider2D boxCollider;

    public void EnableCollision()
    {
        boxCollider.enabled = true;
    }

    void Awake()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        boxCollider.enabled = false;
    }
}

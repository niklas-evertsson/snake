﻿using UnityEngine;

public class SnakeMover : MonoBehaviour
{
    [SerializeField] float moveDelay = 0.1f;
    [SerializeField] InputHandler inputHandler = null;
    [SerializeField] MoveDirection.Direction startDirection = MoveDirection.East;

    bool hasChangedDirection;
    float lastMoveTime = 0f;
    MoveDirection moveDirection;
    TailMover tailMover;
    Vector2 previousHeadPosition;

    void Awake()
    {
        tailMover = GetComponent<TailMover>();
    }

    void Start()
    {
        moveDirection = new MoveDirection(startDirection);
    }

    void Update()
    {
        if(!hasChangedDirection)
        {
            SetDirectionFromInput();
        }

        if(Time.time > lastMoveTime + moveDelay)
        {
            MoveHead();
            tailMover.MoveTail(previousHeadPosition);
            hasChangedDirection = false;
            lastMoveTime = Time.time;
        }
    }

    void MoveHead()
    {
        previousHeadPosition = transform.position;
        transform.Translate(moveDirection.Vector);
    }

    void SetDirectionFromInput()
    {
        if(inputHandler.up)
        {
            hasChangedDirection = moveDirection.SetDirection(MoveDirection.North);
        }
        else if(inputHandler.down)
        {
            hasChangedDirection = moveDirection.SetDirection(MoveDirection.South);
        }
        else if(inputHandler.left)
        {
            hasChangedDirection = moveDirection.SetDirection(MoveDirection.West);
        }
        else if(inputHandler.right)
        {
            hasChangedDirection = moveDirection.SetDirection(MoveDirection.East);
        }
    }
}

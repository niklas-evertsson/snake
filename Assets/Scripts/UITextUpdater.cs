﻿using UnityEngine;
using UnityEngine.UI;

public class UITextUpdater : MonoBehaviour
{
    [SerializeField] IntVariablePositive intVariable = null;

    Text scoreTextComponent = null;
    string scoreText;

    public void UpdateUIText()
    {
        scoreTextComponent.text = scoreText + intVariable.Value;
    }

    void Awake()
    {
        scoreTextComponent = GetComponent<Text>();
        scoreText = scoreTextComponent.text;
        UpdateUIText();
    }
}

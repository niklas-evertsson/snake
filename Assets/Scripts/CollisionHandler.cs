﻿using UnityEngine;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] GameEvent OnEatFood = null;
    [SerializeField] GameEvent OnSnakeDied = null;
    [SerializeField] IntVariablePositive foodInBelly = null;
    [SerializeField] IntVariablePositive score = null;

    void OnTriggerEnter2D(Collider2D collider)
    {
        Food food = collider.GetComponent<Food>();
        if(food != null)
        {
            Destroy(food.gameObject);
            foodInBelly.Add(food.amount);
            score.Add(food.amount);
            OnEatFood.Raise();
        }
        else
        {
            OnSnakeDied.Raise();
        }
    }
}

﻿using UnityEngine;

public struct MoveDirection
{
    public enum Direction
    {
        North,
        East,
        South,
        West
    }
    public const Direction North = Direction.North;
    public const Direction East = Direction.East;
    public const Direction South = Direction.South;
    public const Direction West = Direction.West;

    public Vector2 Vector {get {return vector;}}

    Direction direction;
    Vector2Int vector;

    public MoveDirection(Direction direction)
    {
        this.direction = direction;
        this.vector = GetVectorFromDirection(direction);
    }

    public bool SetDirection(Direction newDirection)
    {
        if((int)direction % 2 == (int)newDirection % 2) return false;

        direction = newDirection;
        vector = GetVectorFromDirection(direction);
        return true;
    }

    static Vector2Int GetVectorFromDirection(Direction direction)
    {
        switch(direction)
        {
            case Direction.North:
                return Vector2Int.up;
            case Direction.East:
                return Vector2Int.right;
            case Direction.South:
                return Vector2Int.down;
            case Direction.West:
                return Vector2Int.left;
            default:
                break;
        }

        Debug.Log(direction.ToString() + " has no corresponding vector, returning a vector of length zero.");
        return Vector2Int.zero;
    }
}

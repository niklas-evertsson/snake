﻿using UnityEngine;

[CreateAssetMenu(fileName = "InputHandler", menuName = "Input Handler")]
public class InputHandler : ScriptableObject
{
    public bool up {get {return Input.GetKeyDown(KeyCode.UpArrow);}}
    public bool down {get {return Input.GetKeyDown(KeyCode.DownArrow);}}
    public bool left {get {return Input.GetKeyDown(KeyCode.LeftArrow);}}
    public bool right {get {return Input.GetKeyDown(KeyCode.RightArrow);}}
}

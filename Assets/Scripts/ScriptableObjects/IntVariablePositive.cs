﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "IntVariablePositive", menuName = "IntVariablePositive")]
public class IntVariablePositive : ScriptableObject
{
    public int Value {get {return value;}}
    [NonSerialized] int value;

    [SerializeField] bool isPersistent = false;
    [SerializeField] int defaultValue = 0;

    string variableName;

    public void Add(int amount)
    {
        value += amount;
        value = value < 0 ? 0 : value;
    }

    public void Set(int amount)
    {
        value = amount;
        value = value < 0 ? 0 : value;
    }

    public void Subtract(int amount)
    {
        value -= amount;
        value = value < 0 ? 0 : value;
    }

    void OnDisable()
    {
        if(isPersistent)
        {
            PlayerPrefs.SetInt(variableName, value);
        }
    }

    void OnEnable()
    {
        variableName = this.name.ToString();
        value = isPersistent ? PlayerPrefs.GetInt(variableName) : defaultValue;
    }
}

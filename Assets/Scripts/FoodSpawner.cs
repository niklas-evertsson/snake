﻿using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    [SerializeField] Food foodPrefab = null;
    [SerializeField] Transform NorthWall = null;
    [SerializeField] Transform EastWall = null;
    [SerializeField] Transform SouthWall = null;
    [SerializeField] Transform WestWall = null;

    public void SpawnFood()
    {
        Vector2 spawnPosition;
        spawnPosition.x = (int)Random.Range(WestWall.position.x + 1, EastWall.position.x - 1);
        spawnPosition.y = (int)Random.Range(SouthWall.position.y + 1, NorthWall.position.y - 1);
        Instantiate(foodPrefab, spawnPosition, Quaternion.identity);
    }

    void Start()
    {
        SpawnFood();
    }
}
